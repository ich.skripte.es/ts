#! /bin/bash

INSTALLERVERSION="1.0.0"
SERVERVERSION="3.1.6"

function greenMessage {
  echo -e "\\033[32;1m${@}\033[0m"
}

function magentaMessage {
  echo -e "\\033[35;1m${@}\033[0m"
}

function cyanMessage {
  echo -e "\\033[36;1m${@}\033[0m"
}

function redMessage {
  echo -e "\\033[31;1m${@}\033[0m"
}

function yellowMessage {
  echo -e "\\033[33;1m${@}\033[0m"
}

function errorQuit {
  errorExit "Exit now!"
}

function errorExit {
  redMessage ${@}
  exit 0
}

function errorContinue {
  redMessage "Invalid option."
  return
}

function pre_initialization {

	if [ "$(id -u)" != "0" ]; then
	  cyanMessage "Change to root account required"
	  su root
	fi

	if [ "$(id -u)" != "0" ]; then
	  errorExit "Still not root, aborting"
	fi

	if [ -f /etc/centos-release ]; then
      yum -y -q install redhat-lsb
	fi

	if [ -f /etc/debian_version ]; then
	  apt-get install debconf-utils -y >/dev/null 2>&1
	  apt-get install lsb-release -y >/dev/null 2>&1
	fi

	MACHINE=$(uname -m)
	OS=$(lsb_release -i 2> /dev/null | grep 'Distributor' | awk '{print tolower($3)}')
	OSBRANCH=$(lsb_release -c 2> /dev/null | grep 'Codename' | awk '{print $2}')

	if [ -z "$OS" ]; then
	  errorExit "Error: Could not detect OS. Currently only Debian, Ubuntu and CentOS are supported. Aborting"!
	fi

	if [ -z "$OSBRANCH" ] && [ -f /etc/centos-release ]; then
	  errorExit "Error: Could not detect branch of OS. Aborting"
	fi

	if [ "$MACHINE" == "x86_64" ]; then
	  ARCHVERSION="amd64"
	else
	  errorExit "$MACHINE is not supported!"
	fi

}

function checkOS {

	if [ -f /etc/debian_version ] || [ -f /etc/centos-release ]; then
	  return 0
	else
	  return 1
	fi

}

function install_dep {

	if [ "$OS" == "debian" ] || [ "$OS" == "ubuntu" ] ; then
	  apt-get install wget screen ca-certificates unzip -y #>/dev/null 2>&1 #
	elif [ "$OS" == "centos" ] ; then
	  dhclient #>/dev/null 2>&1 #
	  yum install wget screen ca-certificates unzip -y #>/dev/null 2>&1 #
	fi

}

function download_tsserver_survey {

  yellowMessage "Where should the TeamSpeak³ server be saved?"
	TSDIR=""
	while [[ ! -d $TSDIR ]]; do
	  read -rp "Directory [/home/ts]: " TSDIR
		if [ "$TSDIR" == "" ] ; then
		  TSDIR=/home/ts
		fi
		if [ ! -d $TSDIR ] ; then
		  mkdir -p "$TSDIR" #>/dev/null 2>&1 #
		  TSDIREMPTY=true
		fi
	done

	if [ -f "$TSDIR/ts3server_startscript.sh" ] ; then

	  redMessage "TeamSpeak³ server installation found. Would you like to update or remove it?"
	  redMessage "Warning: The \"Remove\" option will remove all data of your current installation!"

		OPT1S=("Update" "Remove" "Remove and install" "Quit")
		select OPT1 in "${OPT1S[@]}" ; do
			case "$REPLY" in
			  1|2 ) break;;
			  3 ) errorQuit;;
			  *) errorContinue;;
			esac
		done

		if [[ $OPT1 == "Update" ]]; then
		  update_tsserver
		elif [[ $OPT1 == "Remove" ]] ; then
		  remove_tsserver
		elif [[ $OPT1 == "Remove and install" ]] ; then
		  remove_tsserver
		  install_tsserver
		fi
	elif [ $TSDIREMPTY ] ; then
	  install_tsserver
	else
	
	  redMessage "The installer located files in the directory but didn't find a TeamSpeak³ server installation."
	  yellowMessage "What would you like to do?"
	
		OPT2S=("Remove all files" "Remove all files and install" "Update (overwrite if exists)" "Quit")
		select OPT2 in "${OPT2S[@]}" ; do
			case "$REPLY" in
			1|2|3 ) break;;
			4 ) errorQuit;;
			*) errorContinue;;
			esac
		done

		if [[ $OPT2 == "Remove all files" ]]; then
		  remove_tsserver
		elif [[ $OPT2 == "Remove all files and install" ]] ; then
		  remove_tsserver
		  install_tsserver
		elif [[ $OPT2 == "Update (overwrite if exists)" ]] ; then
		  install_tsserver
		fi
	fi

  echo ""

}

function download_tsserver {

  cd $TSDIR #>/dev/null 2>&1 #
  cyanMessage "Step 1 / 2 complete!"
  rm -rf TeamSpeak3-Client-linux_$ARCHVERSION-$SERVERVERSION.run #>/dev/null 2>&1 #
  wget -q -O $TSDIR/TeamSpeak3-Client-linux_$ARCHVERSION-$SERVERVERSION.run http://dl.4players.de/ts/releases/$SERVERVERSION/TeamSpeak3-Client-linux_$ARCHVERSION-$SERVERVERSION.run #>/dev/null 2>&1 #
  cyanMessage "Step 2 / 2 complete!"

}

function install_tsserver {

  download_tsserver

  yellowMessage "Please enter a username for the bot owner"
	
  TSUSER=""
	while [[ ! $TSUSER ]] ; do
	  read -rp "Username [ts]: " TSUSER
		if [ -z "$TSUSER" ] ; then
		  TSUSER=ts
		fi
	done
	
	if ! id -u $TSUSER > /dev/null 2>&1 ; then
	  useradd -f -1 -m -s /bin/bash -d $TSDIR $TSUSER #>/dev/null 2>&1 #
	else
	  redMessage "test"
	fi

  chown -R $TSUSER:$TSUSER $TSDIR #>/dev/null 2>&1 #
  chmod 744 $TSDIR/TeamSpeak3-Client-linux_$ARCHVERSION-$SERVERVERSION.run #>/dev/null 2>&1 #

  for session in $(screen -ls | grep -o '[0-9]*\.teamspeakinstallersetup'); do screen -S "${session}" -X quit; done
  sleep 1
  rm -rf $TSDIR/.setuplog #>/dev/null 2>&1 #
  touch $TSDIR/.setuplog #>/dev/null 2>&1 #
  echo -e "# Logfile\nlogfile $TSDIR/.setuplog\n# Lastline Info\nautodetach on\nstartup_message off\nhardstatus alwayslastline\nshelltitle 'bash'\nhardstatus string '%{gk}Info: Press ENTER then q then y and lastly ENTER again!'" > $TSDIR/.setupscreenrc
  cd $TSDIR && screen -c "$TSDIR/.setupscreenrc" -LS teamspeakinstallersetup bash -c "bash $TSDIR/TeamSpeak3-Client-linux_$ARCHVERSION-$SERVERVERSION.run"

}

function initial {

  cyanMessage "Installing nessesary dependencies... This may take up to 10 minutes..."
  pre_initialization
	if [ ! checkOS ] ; then
	  redMessage "OS not supported. The installer only supports Debian, Ubuntu and CentOS. Aborting..."
	  exit 1
	fi
  install_dep

  clear
  cd /
  
  greenMessage "This is a TeamSpeak³ Server installer. USE AT YOUR OWN RISK!"
  sleep 2
  magentaMessage "Installer version: $INSTALLERVERSION"
  sleep 1
  redMessage "Script written by Arnim S. @ https://www.eu-hosting.ch"
  sleep 1

  echo ""
  download_tsserver_survey

}

initial